---
workflow:
  rules:
    - if: $CI_MERGE_REQUEST_IID
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    - if: $CI_PIPELINE_SOURCE == "web"
    - if: $CI_PIPELINE_SOURCE == "schedule"

stages:
  - lint
  - build
  - test
  - deploy

lint-yamllint:
  stage: lint
  image:
    name: cytopia/yamllint:latest
    entrypoint: ["/bin/ash", "-c"]
  timeout: 3 minutes
  interruptible: true
  script:
    - yamllint -f colored .

lint-jsonlint:
  stage: lint
  image:
    name: cytopia/jsonlint:latest
    entrypoint: ["/bin/ash", "-c"]
  timeout: 3 minutes
  interruptible: true
  script:
    - >-
      find . -name "*.json" ! -path "*brol*" ! -path "*tmp*"
      | xargs -t -r -n 1 jsonlint -t "    " -q

lint-shellcheck:
  stage: lint
  image: koalaman/shellcheck-alpine:stable
  timeout: 3 minutes
  interruptible: true
  script:
    - find . -name "*.sh" ! -path "*brol*" -print0 | xargs -r -0 shellcheck

.build-dockerimage:
  services:
    - docker:dind
  image: docker:latest
  timeout: 10 minutes
  interruptible: true
  before_script:
    - >- # Login gitlab
      [ -z "$CI_IMAGE_TAG" ] ||
      echo "$CI_REGISTRY_PASSWORD" | docker login $CI_REGISTRY --username $CI_REGISTRY_USER --password-stdin
    - >- # Login dockerhub
      [ -z "$DH_IMAGE_TAG" ] ||
      echo "$DH_REGISTRY_PASSWORD" | docker login $DH_REGISTRY --username $DH_REGISTRY_USER --password-stdin
  script:
    - >- # Build
      docker build -f $BUILD_DOCKERFILE -t mybuild
      --build-arg PHP_VERSION=${BUILD_PHP_VERSION}
      --build-arg PHPUNIT_VERSION=${BUILD_PHPUNIT_VERSION}
      .
    - docker run --rm mybuild phpunit --version
    - echo tags ${CI_IMAGE_TAG} , ${DH_IMAGE_TAG}
    - >- # Push gitlab
      [ -z "$CI_IMAGE_TAG" ] ||
      (docker tag mybuild $CI_IMAGE_TAG && docker push $CI_IMAGE_TAG)
    - >- # Push dockerhub
      [ -z "$DH_IMAGE_TAG" ] ||
      (docker tag mybuild $DH_IMAGE_TAG && docker push $DH_IMAGE_TAG)
  parallel:
    matrix:
      - BUILD_VERSION: "8.3"
        BUILD_DOCKERFILE: "dockerfiles/Dockerfile.cli"
        BUILD_PHP_VERSION: "8.3"
        BUILD_PHPUNIT_VERSION: "11"
      - BUILD_VERSION: "8.4"
        BUILD_DOCKERFILE: "dockerfiles/Dockerfile.cli"
        BUILD_PHP_VERSION: "8.4"
        BUILD_PHPUNIT_VERSION: "11"

build-dockerimage:
  stage: build
  rules:
    - if: "$CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH"
      when: on_success
  variables:
    CI_IMAGE_TAG: $CI_REGISTRY_IMAGE:dev-$BUILD_VERSION-$CI_COMMIT_REF_SLUG
  extends:
    - .build-dockerimage

deploy-dockerimage:
  stage: deploy
  rules:
    - if: "$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH"
      when: on_success
  variables:
    CI_IMAGE_TAG: $CI_REGISTRY_IMAGE:$BUILD_VERSION
    DH_IMAGE_TAG: $DH_REGISTRY/$DH_REGISTRY_USER/$CI_PROJECT_NAME:$BUILD_VERSION
  extends:
    - .build-dockerimage

deploy-dockerhub-description:
  stage: deploy
  rules:
    - if: "$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH"
      when: on_success
  image: peterevans/dockerhub-description:3
  timeout: 2 minutes
  interruptible: true
  variables:
    DOCKERHUB_USERNAME: $DH_REGISTRY_USER
    DOCKERHUB_PASSWORD: $DH_REGISTRY_PASSWORD
    DOCKERHUB_REPOSITORY: $DH_REGISTRY_USER/$CI_PROJECT_NAME
    SHORT_DESCRIPTION: $CI_PROJECT_DESCRIPTION
    README_FILEPATH: $CI_PROJECT_DIR/README.md
  script:
    - /entrypoint.sh
  needs: ["deploy-dockerimage"]
