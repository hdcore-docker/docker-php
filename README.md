# HDCore - docker-php

## Introduction

This is a small container image that contains a php environment with composer and phpunit. This container is very usefull for automatic testing during CI.

## Image usage

- Run phpunit:

```bash
docker run --rm -v /path/to/code:/code hdcore/docker-php:<version> phpunit --coverage-text
```

- Run shell:

```bash
docker run -it --rm -v /path/to/code:/code hdcore/docker-php:<version> /bin/sh
```

- Use in .gitlab-ci.yml:

```bash
image: hdcore/docker-php:<version>
script: phpunit --coverage-text
```

### Add http proxy on startup

- Set the http_proxy (lowercase) environment variable

### Add extra CA certificates on startup

- Add the \*.crt to the /certificates/ folder.
- Set the CACERT_FILE_XXXX environment variable who points to local filename
- Set the CACERT_VAR_XXX environment variable with the content of the certificate

### Add extra SSH KEY on startup

- Set the SSH_PRIVATE_KEY environment variable to the private key content:

### Add composer access tokens

- Set the COMPOSER_AUTH environment variable with the JSON content

## Available tags

- hdcore/docker-php:8.3
- hdcore/docker-php:8.4

## Container Registries

The image is stored on multiple container registries at dockerhub and gitlab:

- Docker Hub:
  - hdcore/docker-php
- Gitlab:
  - registry.gitlab.com/hdcore-docker/docker-php

## Building image

Build:

```bash
docker compose build local-php-8.3
docker compose build local-php-8.4
```

## Running the image

### Default

```bash
docker compose run --rm local-php-8.3
docker compose run --rm local-php-8.4
```
