#!/bin/sh

# Author Danny
# Version GIT: 2021-05-24 15:29

# set-proxy.sh
# Set the proxy based on the environment variable http_proxy
# 
# Compatible with: docker-php only, git

printf "Start set-proxy.sh\n"
printf "Configuring proxy server...\n"

# Fallback for uppercase variable
if [ -n "${HTTP_PROXY}" ]
then
  http_proxy=${HTTP_PROXY}
fi

# Setting or removing proxy server
if [ -n "${http_proxy}" ]
then
  printf "Setting fixed proxy server: %s\n" "${http_proxy}"
  export https_proxy="${http_proxy}"
  hash git 2>/dev/null && git config --global http.proxy "${http_proxy}"
  hash git 2>/dev/null && git config --global https.proxy "${http_proxy}"
else
  printf "Setting empty proxy server\n"
  export https_proxy=""
  hash git 2>/dev/null && git config --global --unset http.proxy
  hash git 2>/dev/null && git config --global --unset https.proxy 
fi

# shellcheck disable=SC2059
printf "Done\n"
