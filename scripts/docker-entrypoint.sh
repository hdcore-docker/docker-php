#!/bin/sh

# Author: Danny Herpol <hdcore@lachjekrom.com>
# Version GIT: 2023-12-23 08:52

# entrypoint.sh
# for hdcore docker-php image

CGREEN="\e[32m"
CNORMAL="\e[0m"

# shellcheck disable=SC2059
printf "== ${CGREEN}Start docker-entrypoint.sh ${CNORMAL} ==\n"

# Show version
printf "HDCore docker-php container image\n"
printf "Installed version of php:\n"
php --version
printf "Installed version of composer:\n"
composer --version
printf "Installed version of phpunit:\n"
phpunit --version

# Setting proxy server
# shellcheck disable=SC1091
. set-proxy.sh

# Adding ca-certificates
# shellcheck disable=SC1091
. set-cacertificates.sh

# Adding SSH keys in current session
# shellcheck disable=SC1091
. set-sshkeys.sh

# shellcheck disable=SC2059
printf "== ${CGREEN}End docker-entrypoint.sh ${CNORMAL} ==\n"

# Execute docker CMD
exec "$@"
