#!/bin/sh

# Author Danny
# Version GIT: 2021-05-24 15:29

# set-sshkeys.sh
# set the SSH key for gitlab/github use

printf "Start set-sshkeys.sh\n"
printf "Configuring ssh key...\n"

# Adding SSH private key
if [ -n "$SSH_PRIVATE_KEY" ]
then
  printf "Adding ssh private key to ssh-agent\n"
  # shellcheck disable=SC2046
  eval $(ssh-agent -s)
  echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null
  mkdir -p ~/.ssh
  chmod 700 ~/.ssh
  ssh-keyscan gitlab.com >> ~/.ssh/known_hosts
  ssh-keyscan github.com >> ~/.ssh/known_hosts
  chmod 644 ~/.ssh/known_hosts
  # [[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config
else
  printf "No SSH_PRIVATE_KEY present\n"
fi

printf "Done\n"
